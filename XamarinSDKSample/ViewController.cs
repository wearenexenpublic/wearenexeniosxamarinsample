﻿using System;
using System.Diagnostics;
using CoreBluetooth;
using CoreGraphics;
using CoreLocation;
using Foundation;
using NXBeaconSDK;
using UIKit;

namespace XamarinSDKSample
{
    public partial class ViewController : UIViewController
    {
        CBCentralManager bluetoothManager;
        public UIActivityIndicatorView activityIndicatorView;
        ManagerContext managerContext;

        protected ViewController(IntPtr handle) : base(handle)
        {
            managerContext = ManagerContext.Instance;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            bluetoothManager = new CBCentralManager(CoreFoundation.DispatchQueue.MainQueue);

            if (CLLocationManager.Status == CLAuthorizationStatus.AuthorizedAlways || CLLocationManager.Status == CLAuthorizationStatus.AuthorizedWhenInUse)
            {
                managerContext.StartManager(StartProgress);
            }
            else
            {
                var locMgr = new CLLocationManager();
                locMgr.PausesLocationUpdatesAutomatically = false;
                locMgr.AuthorizationChanged += AuthorizationChanged;
                if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                {
                    locMgr.RequestAlwaysAuthorization();
                }

                if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
                {
                    locMgr.AllowsBackgroundLocationUpdates = true;
                }
            }

            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        private void StartProgress()
        {
            activityIndicatorView = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
            activityIndicatorView.Frame = new CGRect(150, 150, 40, 40);
            activityIndicatorView.Center = View.Center;
            View.AddSubview(activityIndicatorView);
            activityIndicatorView.BringSubviewToFront(View);

            activityIndicatorView.Color = UIColor.FromRGB(39, 170, 225);
            activityIndicatorView.StartAnimating();
        }

        private void AuthorizationChanged(object sender, CLAuthorizationChangedEventArgs e)
        {
            if (e.Status == CLAuthorizationStatus.AuthorizedAlways || e.Status == CLAuthorizationStatus.AuthorizedWhenInUse)
                managerContext.StartManager(StartProgress);
        }
    }
}
